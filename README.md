# The challenge to use multi-temporal InSAR for early warning

This repository supplements the comment by
Matthias Schlögl <sup>[![](https://info.orcid.org/wp-content/uploads/2020/12/orcid_16x16.gif)](https://orcid.org/0000-0002-4357-523X)</sup>,
Karlheinz Gutjahr and
Sven Fuchs <sup>[![](https://info.orcid.org/wp-content/uploads/2020/12/orcid_16x16.gif)](https://orcid.org/0000-0002-0644-2876)</sup> (2022): **The challenge to use multi-temporal InSAR for landslide early warning**. *Natural Hazards*. [doi:10.1007/s11069-022-05289-9](https://doi.org/10.1007/s11069-022-05289-9)


## Highlights

> Satellite radar interferometry is a powerful tool for measuring displacements
> of the Earth's surface. However, we recommend to extend the currently prevailing
> focus on ex-post analyses and monitoring towards ex-ante early warning applications.
> Underlying challenges and key requirements are discussed.


## Data sources

- PSI data:
    - The comment is supplemented with an analysis of the quick-clay landslide that
      occurred in Gjerdrum on 2020-12-30, using official data from
      [INSAR Norway](https://www.ngu.no/en/topic/insar-norway).
    - PSI time series data can be downloaded from [insar.ngu.no](https://insar.ngu.no/)
      using the polygon select tool.
    - Direct link to the event:
      [Gjerdrum event 2020-12-30](https://insar.ngu.no/#llh=11.04564910,60.06106166,1823.55135076&look=-0.09561120,-0.86655732,-0.48983355&right=0.98149988,-0.00006636,-0.19146275&up=-0.16588095,0.49907756,-0.85053225&layers=nma-topo,in-asc1,in-asc2,in-dsc1,in-dsc2).
    - The considered orbits are:
        - ascending: 146, 192
        - descending: 66, 139
- The outline of the event polygon is taken from
  [OpenStreetMap](https://www.openstreetmap.org/relation/12119561#map=15/60.0607/11.0481).
- The elevation model is available from [Høydedata](https://hoydedata.no/LaserInnsyn/).
  The DTM 1 product (digital terrain model with 1 m spatial resolution) was used.
- Precipitation data are extracted from the Nordic Gridded Climate Dataset ([NGCD](https://surfobs.climate.copernicus.eu/dataaccess/access_ngcd.php)),
  available trough the [MET Norway Thredds Service](https://thredds.met.no/thredds/catalog/ngcd/catalog.html).


## Data exploration

- Structure of the repo:
    - `dat`: data sets
    - `dev`: development/code
    - `doc`: documentation
    - `plt`: plots

- The repo structure is loosely based on the
  [Cookiecutter Data Science](https://drivendata.github.io/cookiecutter-data-science/)

- Exploratory analysis of the data was done in **R**, mainly using
  [`tidyverse`](https://www.tidyverse.org/) and
  [`sf`](https://r-spatial.github.io/sf/).
  The following R packages are required to run all the code in this repo:

  ```R
  install.packages("tidyverse")
  install.packages("sf")
  install.packages("stars")
  install.packages("ggspatial")
  install.packages("ggnewscale")
  install.packages("ggalt")
  install.packages("colorspace")
  ```
  Additional DEM data processing was done directly via [`gdal`](https://gdal.org/).

- The coding style adheres to the [tidyverse style guide](https://style.tidyverse.org/)
  using [`styler`](https://styler.r-lib.org/).
