#!/usr/bin/env zsh

echo "clip dem"
gdalwarp -of GTiff -cutline dat/interim/bbox_UTM33N.gpkg -cl bbox_UTM33N -crop_to_cutline dat/raw/dtm1/data/dtm1_33_125_115.tif dat/interim/dem_bbox.tif

echo "compute hillshade"
gdaldem hillshade dat/interim/dem_bbox.tif dat/interim/hillshade_bbox.tif -of GTiff -b 1 -z 1.3 -s 1.0 -az 315.0 -alt 45.0
