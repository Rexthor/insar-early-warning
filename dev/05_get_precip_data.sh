#!/usr/bin/zsh

echo_timestamp() {
    date +"%Y-%m-%dT%H:%M:%S $@"
}

for day in {01..31}
do
    echo_timestamp "» Downloading day: 2020-12-$day"
    wget https://thredds.met.no/thredds/fileServer/ngcd/version_21.03/RR/type2/2020/12/NGCD_RR_type2_202012$day.nc -P dat/raw/ngcd/
done
